const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Cocktail = require('../models/Cocktail');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  // Cocktail index
  router.get('/', (req, res) => {
    Cocktail.find({published: true}).populate('user')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  // Cocktail create
  router.post('/', [auth, upload.single('image')], (req, res) => {
    const cocktailData = req.body;

    if (req.file) {
      cocktailData.image = req.file.filename;
    } else {
      cocktailData.image = null;
    }

    cocktailData.ingredients = JSON.parse(cocktailData.ingredients);
    cocktailData.user = req.user._id;

    const cocktail = new Cocktail(cocktailData);

    cocktail.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/admin', auth, permit('admin'), (req, res) => {
    Cocktail.find().populate('user')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  // cocktail get by ID
  router.get('/:id', (req, res) => {
    Cocktail.findOne({_id: req.params.id}).populate('user')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });


  router.put('/:id', [auth, permit('admin')], async (req, res) => {
    console.log(req.params.id);
    console.log('here');
    const cocktail = await Cocktail.findOne({_id: req.params.id});
    cocktail.published = true;
    cocktail.save()
      .then(cocktail => res.send(cocktail))
      .catch(error => res.status(400).send(error));
  });

  router.put('/:id/rate', auth, async (req, res) => {
console.log(req.body.rate);

    const cocktail = await Cocktail.findOneAndUpdate({_id: req.params.id}, {$push: {rate: req.body.rate}});
    cocktail.published = true;
  });

  router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const cocktail = await Cocktail.findOneAndRemove({_id: req.params.id});
    res.send(cocktail)
  });

  return router;
};

module.exports = createRouter;