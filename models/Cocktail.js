const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CocktailSchema = new Schema({
  title: {
    type: String, required: true
  },
  recipe: String,
  image: String,
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  published: {
    type: Boolean,
    default: false
  },
  ingredients: [
    {
      title: String,
      amount: String
    }
  ],
  rate: [
    {
      user:{
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
      },
      rate: Number
    }
  ]
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;