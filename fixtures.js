const mongoose = require('mongoose');
const config = require('./config');

const Cocktail = require('./models/Cocktail');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('cocktails');
    await db.dropCollection('users');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [userUsername, adminUsername] = await User.create({
    username: 'user',
    password: '123',
    role: 'user'
  }, {
    username: 'admin',
    password: 'admin123',
    role: 'admin'
  });


  await Cocktail.create({
    title: 'Mahito',
    recipe: 'Мохи́то — коктейль на основе светлого рома и листьев мяты. Происходит с острова Куба, стал популярен в США в 1980-х. Коктейль входит в список «современной классики» международной ассоциации барменов и классифицируется как лонг дринк',
    ingredients: [{title: 'лайм', amount: '1/2'},
      {title: 'мяты', amount: '10 листиков'},
      {title: 'сахара', amount: '2 ст.л'}],
    user: userUsername._id,
    image: 'mahito.jpg',
    rate: [{user: userUsername._id, rate: '7'},
      {user: userUsername._id, rate: '5'}],
    published: true
  }, {
    title: 'Лонг-Айленд',
    recipe: 'Ло́нг-А́йленд айс ти — популярный коктейль на основе водки, джина, текилы и рома. Кроме того, в состав коктейля обычно входит трипл сек, кола и так далее. «Лонг-Айленд» — один из самых крепких коктейлей. Классифицируется как лонг дринк.',
    ingredients: [{title: 'водка', amount: ' 20 мл'},
      {title:'золотой ром', amount: ' 20 мл'}],
    user: adminUsername._id,
    image: 'long.jpg',
    rate: [{user: adminUsername._id, rate: '5'},
      {user: adminUsername._id, rate: '9'}],
    published: false
  });

  db.close();
});